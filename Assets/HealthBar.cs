﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{

    public float currentHealth = 100;
    public float fullHealth = 100;

    private SpriteRenderer bar;
    private float startingWidth;

	void Start ()
    {
        bar = transform.Find("bar").GetComponent<SpriteRenderer>();
        startingWidth = bar.size.x;
	}

	void Update ()
    {
        var targetWidth = (currentHealth / fullHealth) * startingWidth;
        bar.size = new Vector2(Mathf.Lerp(bar.size.x, targetWidth, 0.1f), bar.size.y);
	}
}
