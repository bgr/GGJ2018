﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemyPrefabs;
    public GameObject[] healthPrefabs;
    public float healthChance = 0.1f;
    public int numSpawned = 0;
    public int currentMaxAllowed = 1;
    public int absoluteMaxAllowed = 8;
    public int maxAllowedIncreaseTime = 10;

    private Camera cam;
    //private List<Transform> liveEnemies = new List<Transform>();


	void Start ()
    {
        cam = Camera.main;

        StartCoroutine(SpawnCoro());
        StartCoroutine(IncreaseDifficulty());
	}

    IEnumerator IncreaseDifficulty ()
    {
        var wait = new WaitForSeconds(maxAllowedIncreaseTime);

        while (currentMaxAllowed < absoluteMaxAllowed)
        {
            yield return wait;
            currentMaxAllowed++;
        }
    }

    IEnumerator SpawnCoro ()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            if (numSpawned < currentMaxAllowed)
            {
                Spawn();
            }
        }
    }
	
	void Spawn ()
    {
        var isHealth = Health.instance.health < 90 && Random.Range(0f, 1f) <= healthChance;
        var arr =  isHealth ? healthPrefabs : enemyPrefabs;
        var obj = Instantiate(arr[Random.Range(0, arr.Length)]);
        var pos = cam.ScreenToWorldPoint(new Vector3(Random.Range(20, Screen.width - 20), Screen.height + 40, 0));
        pos.z = 0;
        obj.transform.position = pos;

        obj.GetComponent<DamageDetector>().EnemyDestroyed += OnDestroyed;

        if (!isHealth) numSpawned++;
	}

    void OnDestroyed (DamageDetector enemy)
    {
        if (!enemy.isHealthPack) numSpawned--;
    }
}
