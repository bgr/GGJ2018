﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTimeout : MonoBehaviour
{
    public float seconds = 5;

	void Start ()
    {
        StartCoroutine(Kill());
	}
	
	IEnumerator Kill()
    {
        yield return new WaitForSeconds(seconds);
        Destroy(gameObject);
    }
}
