﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class RadialsImageEffect : MonoBehaviour
{
    public Material mat;
    public Transform shipA;
    public Transform shipB;
    [Range(2, 25)] public float freqA;
    [Range(2, 25)] public float freqB;
    [Range(0, 1)] public float waveStrengthA;
    [Range(0, 1)] public float waveStrengthB;
    [Range(0, 1)] public float damageZoneStrength;
    //[Range(0, 4)] public int DownRes;

    private List<DamageDetector> damageDetectors = new List<DamageDetector>();

    private Texture2D tex;
    private RenderTexture justDamage;


    void Start()
    {
        tex = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
        justDamage = new RenderTexture(tex.width, tex.height, 0, RenderTextureFormat.ARGB32);
    }

    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        //int width = src.width >> DownRes;
        //int height = src.height >> DownRes;

        //RenderTexture rt = RenderTexture.GetTemporary(width, height);
        Graphics.Blit(src, dst, mat);
        Graphics.Blit(justDamage, justDamage, mat);
        //Graphics.Blit(src, dst);

        //RenderTexture.ReleaseTemporary(rt);

    }

    void OnPostRender()
    {
        RenderTexture prev = RenderTexture.active;
        RenderTexture.active = justDamage;
        tex.ReadPixels(new Rect(0, 0, justDamage.width, justDamage.height), 0, 0, false);
        RenderTexture.active = prev;

        UpdateDamages();
    }

    public void RegisterEnemy(DamageDetector d)
    {
        damageDetectors.Add(d);
    }

    void UpdateDamages()
    {

        foreach (var d in damageDetectors)
        {
            var enemyPos = Camera.main.WorldToScreenPoint(d.transform.position);
            var samplingPosX = (int)enemyPos.x + Random.Range(-5, 5);
            var samplingPosY = (int)enemyPos.y + Random.Range(-5, 5);
            d.TakeDamage(tex.GetPixel(samplingPosX, samplingPosY));
        }

        damageDetectors.Clear();
    }

    void Update()
    {
        var cam = GetComponent<Camera>();
        mat.SetFloat("_XA", (shipA.position.x - cam.transform.position.x) / cam.orthographicSize);
        mat.SetFloat("_YA", (shipA.position.y - cam.transform.position.y) / cam.orthographicSize);
        mat.SetFloat("_XB", (shipB.position.x - cam.transform.position.x) / cam.orthographicSize);
        mat.SetFloat("_YB", (shipB.position.y - cam.transform.position.y) / cam.orthographicSize);
        mat.SetFloat("_FreqA", freqA * cam.orthographicSize);
        mat.SetFloat("_FreqB", freqB * cam.orthographicSize);
        mat.SetFloat("_WaveStrengthA", waveStrengthA);
        mat.SetFloat("_WaveStrengthB", waveStrengthB);
        mat.SetFloat("_DamageZoneMult", damageZoneStrength);
    }
}