﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public const float minFreq = 3;
    public const float maxFreq = 20;
    public const float freqSweepSpeed = 0.09f;

    public int playerNo = 1;

    private Rigidbody2D rb;
    private string horizontal;
    private string vertical;
    private string freqUp;
    private string triggers;
    private RadialsImageEffect radials;

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        radials = Camera.main.GetComponent<RadialsImageEffect>();
        horizontal = "P" + playerNo + "_Horizontal";
        vertical = "P" + playerNo + "_Vertical";
        freqUp = "P" + playerNo + "_FreqUp";
        triggers = "JoyTriggers";
	}

    void OnTriggerEnter2D (Collider2D coll)
    {
        if (coll.gameObject.GetComponent<DamageDetector>().isHealthPack)
        {
            Health.instance.UpdateHealth(+50);
            Destroy(coll.gameObject);
        }
    }

	void Update ()
    {
        rb.AddForce(new Vector2(Input.GetAxis(horizontal), Input.GetAxis(vertical)));

        float freqOffset = 0;
        if (playerNo == 1)
        {
            if (Input.GetButton(freqUp))
            {
                freqOffset = -freqSweepSpeed;
            }
            else if (Input.GetAxis(triggers) >= 0.2)
            {
                freqOffset = freqSweepSpeed;
            }

            radials.freqA = Mathf.Clamp(radials.freqA + freqOffset, minFreq, maxFreq);
        }
        else if (playerNo == 2)
        {
            if (Input.GetButton(freqUp))
            {
                freqOffset = -freqSweepSpeed;
            }
            else if (Input.GetAxis(triggers) <= -0.2)
            {
                freqOffset = freqSweepSpeed;
            }

            radials.freqB = Mathf.Clamp(radials.freqB + freqOffset, minFreq, maxFreq);
        }
        else
        {
            throw new System.Exception("Only 2 players supported");
        }
	}
}